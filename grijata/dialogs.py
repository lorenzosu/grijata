try:
    # Importing the usual GObject Introspection
    import gi
    gi.require_version("Gtk", "3.0")
    from gi.repository import Gtk, GLib
except ImportError:
    # Importing a pure-python version of GObject Introspection
    # for use inside virtualenvs
    import pgi
    pgi.require_version("Gtk", "3.0")
    from pgi.repository import Gtk, GLib


def jack_lsp_error(parent):
    """ Dialog triggered if there was a jack_lsp error and most probably jack is
    not running.
    """
    dialog = Gtk.MessageDialog(
                parent,
                0,
                Gtk.MessageType.ERROR,
                Gtk.ButtonsType.OK,
                "jack_lsp returned an error.\nIs jack runing??"
            )
    dialog.run()
    dialog.destroy()

