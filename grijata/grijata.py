#!/usr/bin/env python

import sys
import psutil
import commands
import dialogs

try:
    # Importing the usual GObject Introspection
    import gi
    gi.require_version("Gtk", "3.0")
    from gi.repository import Gtk, GLib
except ImportError:
    # Importing a pure-python version of GObject Introspection
    # for use inside virtualenvs
    import pgi
    pgi.require_version("Gtk", "3.0")
    from pgi.repository import Gtk, GLib

class MainWindow(Gtk.Window):
    """ Main class for the grijata window """
    def __init__(self, initial_graph):

        self.custom_icon_size = Gtk.IconSize.register("custom", 48, 48)
        # Main window
        Gtk.Window.__init__(self)
        self.set_default_size(800, 600)
        self.set_position(Gtk.WindowPosition.CENTER_ALWAYS)
        
        self.scroll_window = Gtk.ScrolledWindow()
        self.graph = initial_graph

        self.connected_list = []
        self.connect_grid = None
        self.selected_out_client = ''
        self.selected_in_client = ''

        # Main grid for the window layout (*not* the grid with jack connections)
        self.main_grid = Gtk.Grid()
        self.main_grid.set_orientation(Gtk.Orientation.VERTICAL)

        # Make the top bar and add it to the grid
        self.top_bar = self.make_top_bar()
        self.main_grid.add(self.top_bar)

        # this will trigger the creation of the jack connections grid
        self.outs_combo.set_active(0)
        self.ins_combo.set_active(0)

        # Update the graph again
        self.graph_updated(True)
        self.refresh_grid(True)

        self.add(self.main_grid)


    def jack_is_running(self):
        """ Check if there's a jack process running. self.jack_proc_names is a
        list of possible names for the jack process """
        for p in psutil.process_iter():
            if p.name in self.jack_proc_names:
                print "---------------",p.name
                return True
        # In case no process was found the for ends and so we return False:
        return False

    def refresh_grid(self, first_run):
        """ This refreshes the grid, re-detecting all clients and connections """
        #if first_run != True:
        #    self.connect_grid.destroy()
        #    self.scroll_window.destroy()
        
        self.scroll_window.set_vexpand(True)
        self.scroll_window.set_hexpand(True)
        self.connect_grid = self.make_grid(
                    self.selected_out_client,
                    self.selected_in_client,
                    first_run
                )
        
        self.scroll_window.add_with_viewport(self.connect_grid)

        self.main_grid.add(self.scroll_window)
        #self.scroll_window.set_min_content_height(self.get_allocated_height()-20)
        self.show_all()
    
    def make_grid(self, the_out_cl, the_in_cl, first_run):
        """ Make a Gkt grid layout with buttons for jack ports """
        if first_run != True:
            self.connect_grid.destroy()
        grid = Gtk.Grid()
        self.connected_list = []

        # Just put an empty button at 0,0 (top left) of the grid
        empty_label = Gtk.Label("")
        grid.add(empty_label)
        the_out_port_list = self.graph.clients_out_dic[the_out_cl]
        the_in_port_list = self.graph.clients_in_dic[the_in_cl]

        # First row that shows inputs
        for x, ins in enumerate(the_in_port_list):
            in_pt_string = ins.port_name.lstrip()
            in_tooltip = in_pt_string
            label_text = in_pt_string[0:25].ljust(25)
            label_in_pt = Gtk.Label(label_text)
            label_in_pt.set_tooltip_text(in_tooltip)
            label_in_pt.set_angle(50.0)
            grid.attach(label_in_pt, x+1,0,1,1)

        # First column that show outpus
        for y, outs in enumerate(the_out_port_list):
            out_pt_string = outs.port_name.lstrip()
            out_tooltip = out_pt_string
            if len(out_pt_string) > 20:
                out_pt_string = out_pt_string[0:20] + "..."
            label_text = out_pt_string
            label_out_pt = Gtk.Label(label_text)
            label_out_pt.set_tooltip_text(out_tooltip)
            grid.attach(label_out_pt, 0, y+1, 1, 1)

        for row_count, outs in enumerate(the_out_port_list):
            for col_count, ins in enumerate(the_in_port_list):
                in_port_button = Gtk.Button()

                tip_string = (
                        outs.full_port_name +
                        u"\n     \u2193\n" +
                        ins.full_port_name
                        )
                in_port_button.set_tooltip_text(tip_string)
                if ins.full_port_name in outs.connections:
                    image = Gtk.Image.new_from_stock(
                                Gtk.STOCK_YES,
                                self.custom_icon_size
                            )
                    in_port_button.set_image(image)
                    # if already connected add to the global list of connected ports.
                    self.connected_list.append(
                            outs.full_port_name + " " + ins.full_port_name
                            )
                else:
                    image = Gtk.Image.new_from_stock(
                            Gtk.STOCK_NO,
                            self.custom_icon_size
                            )
                    in_port_button.set_image(image)
                
                in_port_button.connect("clicked", self.button_grid_press, outs, ins)

                grid.attach(in_port_button, col_count + 1, row_count + 1, 1, 1)

        return grid

    def graph_updated(self, first_run):
        """ Run the run the command and parse the jack_lsp command.
            If the graph has changed the self.graph will be update an True
            return. Otherwise False is returned.
        """

        new_graph = commands.get_current_graph()
        # TODO In theory jack could also be closed *after* so this should really
        # be something like 'Exit' or 'Retry'
        if new_graph is None:
            dialogs.jack_lsp_error(self)
            sys.exit(1);

        if (new_graph != self.graph) or (first_run):
            self.graph = new_graph
            return True
        else:
            return False

    def make_top_bar(self):
        """ Make the GUI top bar """
        top_grid = Gtk.Grid()
        
        out_label = Gtk.Label("Out Clients: ")
        top_grid.add(out_label)

        self.outs_combo = Gtk.ComboBoxText()
        self.outs_combo.set_entry_text_column(0)
        self.outs_combo.connect("changed", self.on_outs_combo_changed)
        for out_cli in self.graph.clients_out_dic.keys():
            self.outs_combo.append_text(out_cli)
        top_grid.add(self.outs_combo)

        in_label = Gtk.Label("In Clients: ")
        top_grid.add(in_label)

        self.ins_combo = Gtk.ComboBoxText()
        self.ins_combo.set_entry_text_column(0)
        self.ins_combo.connect("changed", self.on_ins_combo_changed)
        for in_cli in self.graph.clients_in_dic.keys():
            if in_cli.find('system') < 0:
                self.ins_combo.append_text(in_cli)
            else:
                has_system = True
        if has_system:
            self.ins_combo.prepend_text('system')
        top_grid.add(self.ins_combo)
        return top_grid

    
    def button_grid_press(self, the_button, out_p, in_p):
        """ Act when grid button clicked """
        print(out_p.full_port_name + " -> " + in_p.full_port_name)
        connect_string = out_p.full_port_name + " " + in_p.full_port_name
        if connect_string in self.connected_list:
            list_operation = list.remove
            command = commands.disconnect_ports
            image = Gtk.Image.new_from_stock(Gtk.STOCK_NO, Gtk.IconSize.BUTTON)
        else:
            list_operation = list.append
            command = commands.connect_ports
            image = Gtk.Image.new_from_stock(Gtk.STOCK_YES, Gtk.IconSize.BUTTON)

        error = command(out_p, in_p)
        if error is None:
            # Here we're doing either a list.remove or a list.append...
            list_operation(self.connected_list, connect_string)
            the_button.set_image(image)
        else:
            print("ERROR connecting ports %s" % error)
            err_dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR,
                Gtk.ButtonsType.CANCEL, "Error connecting ports"
                )
            err_dialog.format_secondary_text(
                    out_p.full_port_name + "\n|\n" + in_p.full_port_name
                    )
            err_dialog.run()
            err_dialog.destroy()

    def on_outs_combo_changed(self, outs_combo):
        text = outs_combo.get_active_text()
        if text != None:
            print("Selected out client: %s" % text)
            self.selected_out_client = text
        self.refresh_grid(False)


    def on_ins_combo_changed(self, ins_combo):
        text = ins_combo.get_active_text()
        if text != None:
            print("Selected in client: %s" % text)
            self.selected_in_client = text
        self.refresh_grid(False)

    def graph_did_update(self, new_graph):
        if new_graph == self.graph:
            return

        # TODO: refresh dropdowns
        self.graph = new_graph
        self.refresh_grid(False)

    def got_error(self, graph):
        print "error"

        dialogs.jack_lsp_error(self)
        sys.exit(1)

class PatchbayUpdateListener:
    def __init__(self):
        self.main_window = None


    def deliver_event(self, name, payload):
        def event_callback(event_name, payload):
            def default_handler(*args):
                print "Unhandled event: %s\n%s" % args

            callback = getattr(self, event_name, default_handler)
            callback(payload)

            return False # no, glib, don't run this again, PLEASE

        # deliver_event will NOT be called from the mainthread!
        GLib.idle_add(event_callback, name, payload)


    def graph_updated(self, new_graph):
        if self.main_window == None:
            # This is the first update, create the window
            self.main_window = MainWindow(new_graph)
            self.main_window.connect("delete-event", Gtk.main_quit)
            self.main_window.show_all()
        else:
            # this is a regular update, give the graph to the window
            self.main_window.graph_did_update(new_graph)


    def error(self, error):
        if self.main_window == None:
            # This is the first update, but presenting dialogs here will
            # warn on the console...
            dialogs.jack_lsp_error(None)
            sys.exit(1)
        else:
            # normally the window's got a good dialog handler
            self.main_window.got_error(error)


def main():
    event_listener = PatchbayUpdateListener()
    commands.listen_to_graph_updates(event_listener.deliver_event)
    Gtk.main()


if __name__ == '__main__':
   main()

